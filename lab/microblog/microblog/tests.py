# -*- coding: utf-8 -*-

import unittest

from pyramid import testing


####################
#   Testy modeli   #
####################


class BlogModelTests(unittest.TestCase):

    def _getTargetClass(self):
        from .models import Blog
        return Blog

    def _makeOne(self):
        return self._getTargetClass()()

    def test_it(self):
        blog = self._makeOne()
        self.assertEqual(blog.__parent__, None)
        self.assertEqual(blog.__name__, None)


class PostModelTests(unittest.TestCase):

    def _getTargetClass(self):
        from .models import Post
        return Post

    def _makeOne(self, title=u'some data', content =u'some data' ):
        return self._getTargetClass()(title=title,content=content )

    def test_constructor(self):
        instance = self._makeOne()
        self.assertEqual(instance.title, u'some data')


class AppmakerTests(unittest.TestCase):

    def _callFUT(self, zodb_root):
        from .models import appmaker
        return appmaker(zodb_root)

    def test_it(self):
        root = {}
        self._callFUT(root)
        self.assertEqual(root['app_root']['Start'].data,
                         'To jest strona startowa')


#####################
#   Testy widokĂłw   #
#####################


class ViewBlogTests(unittest.TestCase):
    def test_it(self):
        from .views import view_blog
        context = testing.DummyResource()
        request = testing.DummyRequest()
        response = view_blog(context, request)
        self.assertEqual(response.location, 'http://194.29.175.240:31012/')


class ViewPostTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import view_post
        return view_post(context, request)

    def test_it(self):
        blog = testing.DummyResource()
        blog[u'NaprawdÄIstnieje'] = testing.DummyResource()
        context = testing.DummyResource(data=u'Witaj, OkropnyĹwiat NaprawdÄIstnieje')
        context.__parent__ = blog
        context.__name__ = 'strona'
        request = testing.DummyRequest()
        info = self._callFUT(context, request)
        self.assertEqual(info['page'], context)
        self.assertEqual(
            info['content'],
            u'<div class="document">\n'
            u'<p>Witaj, <a href="http://example.com/add_page/Okropny%C5%9Awiat">'
            u'OkropnyĹwiat</a> '
            u'<a href="http://example.com/Naprawd%C4%99Istnieje/">'
            u'NaprawdÄIstnieje</a>'
            u'</p>\n</div>\n')
        self.assertEqual(info['edit_url'],
                         'http://example.com/strona/edit_page')


class AddPostTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import add_post
        return add_post(context, request)

    def test_it_notsubmitted(self):
        context = testing.DummyResource()
        request = testing.DummyRequest()
        request.subpath = ['InnaStrona']
        info = self._callFUT(context, request)
        self.assertEqual(info['post'].data,'')
        self.assertEqual(
            info['save_url'],
            request.resource_url(context, 'add_post', 'InnaStrona'))

    def test_it_submitted(self):
        context = testing.DummyResource()
        data = {'form.submitted':True, 'body':'Siemanko!'}
        request = testing.DummyRequest(data, post=data)
        request.subpath = ['AnotherPage']
        self._callFUT(context, request)
        page = context['AnotherPage']
        self.assertEqual(page.data, 'Siemanko!')
        self.assertEqual(page.__name__, 'AnotherPage')
        self.assertEqual(page.__parent__, context)