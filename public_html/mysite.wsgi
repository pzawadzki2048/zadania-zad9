import os, sys

activate_this = '/home/p12/pyramid2/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from pyramid.paster import get_app, setup_logging
ini_path = '/home/p12/lab/microblog/production.ini'
setup_logging(ini_path)
application = get_app(ini_path, 'main')